import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final bool shouldAlwaysShowScrollbar = false;
  late String selectMode;
  List<String>? modeList;
  late LegendItemOverflowMode overflowMode;
  late String selectedPosition;
  List<String>? PositionedList;
  late LegendPosition position;

  @override
  void initState() {
    selectMode = 'wrap';
    modeList = ['wrap', 'none', 'scroll'].toList();
    position = LegendPosition.auto;
    overflowMode = LegendItemOverflowMode.wrap;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Employee Salaries'),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          if (orientation == Orientation.portrait) {
            return _buildListView();
          } else {
            return _buildCircularChart();
          }
        },
      ),
    );
  }

  ListView _buildListView() {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, index) {
        final employee = ChartSampleData(
          'Employee ${index + 1}',
          [25.0, 35.0, 20.0, 15.0, 5.0][index],
        );
        return ListTile(
          title: Text(employee.x),
          subtitle: Text('Salary: ${employee.y}%'),
        );
      },
    );
  }

  SfCircularChart _buildCircularChart() {
    final List<ChartSampleData> chartData = [
      ChartSampleData('John', 25.0),
      ChartSampleData('Alice', 35.0),
      ChartSampleData('Bob', 20.0),
      ChartSampleData('Emily', 15.0),
      ChartSampleData('Anna', 5.0),
    ];
    return SfCircularChart(
      legend: Legend(
        position: position,
        isVisible: true,
        overflowMode: overflowMode,
        shouldAlwaysShowScrollbar: shouldAlwaysShowScrollbar,
      ),
      tooltipBehavior: TooltipBehavior(enable: true),
      series: <DoughnutSeries<ChartSampleData, String>>[
        DoughnutSeries<ChartSampleData, String>(
          dataSource: chartData,
          xValueMapper: (ChartSampleData data, _) => data.x,
          yValueMapper: (ChartSampleData data, _) => data.y,
          dataLabelSettings: const DataLabelSettings(
            isVisible: true,
            labelPosition: ChartDataLabelPosition.outside,
            labelAlignment: ChartDataLabelAlignment.outer,
            labelIntersectAction: LabelIntersectAction.none,
            textStyle: TextStyle(color: Colors.black),
          ),
        ),
      ],
    );
  }
}

class ChartSampleData {
  final String x;
  final double y;

  ChartSampleData(this.x, this.y);
}
